import org.junit.Assert;
import org.junit.Test;
import ru.sbrf.edu.CustomArrayImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CustomArrayImplTest {

    @Test
    public void sizeTest() {
        CustomArrayImpl<String> strArray = new CustomArrayImpl<>();
        String[] str = new String[]{"aaa", "bbb", "ccc"};
        strArray.addAll(str);
        Assert.assertEquals(3, strArray.size());
    }

    @Test
    public void isEmptyTest() {
        CustomArrayImpl<Integer> intArray = new CustomArrayImpl<>(Arrays.asList(new Integer[]{1, 3, 5}));
        Assert.assertFalse(intArray.isEmpty());
        CustomArrayImpl<String> str = new CustomArrayImpl<>();
        Assert.assertTrue(str.isEmpty());
    }

    @Test
    public void addTest() {
        CustomArrayImpl customArray = new CustomArrayImpl();
        Integer[] arr = {1, 2, 3, 4};
        Assert.assertTrue(customArray.add(arr));
    }

    @Test
    public void addAllArrTest() {
        CustomArrayImpl customArray = new CustomArrayImpl();
        String[] arrElemEmpty = {"1", "2", null,};
        String[] arrEmpty = {null};
        String[] arrNotEmpty = {"Asc", "A123", "sdsdw"};

        boolean isIllegalArgumentEx = false;
        try {
            customArray.addAll(arrEmpty);
        } catch (IllegalArgumentException e) {
            isIllegalArgumentEx = true;
        }
        Assert.assertTrue(isIllegalArgumentEx);
        isIllegalArgumentEx = false;

        try {
            customArray.addAll(arrElemEmpty);
        } catch (IllegalArgumentException e) {
            isIllegalArgumentEx = true;
        }
        Assert.assertTrue(isIllegalArgumentEx);
        isIllegalArgumentEx = false;

        try {
            customArray.addAll(arrNotEmpty);
        } catch (IllegalArgumentException e) {
            isIllegalArgumentEx = true;
        }
        Assert.assertFalse(isIllegalArgumentEx);
    }

    @Test
    public void addAllColTest() {
        CustomArrayImpl<String> listCustom = new CustomArrayImpl<>();
        List<String> list = new ArrayList<>();
        list.add("One");
        list.add("Two");
        list.add("Three");
        listCustom.addAll(list);
        Assert.assertEquals(listCustom.toArray(), list.toArray());

    }

    @Test
    public void addAllArrIndexTest() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        String[] arrStr = {"abc", "aa", "ddd", "", "dodo"};
        String[] arrEmpty = {null};
        customArray.addAll(arrStr);

        Assert.assertTrue(customArray.addAll(2, arrStr));

        boolean isArrayIndexOutOfBoundsExceptionEx = false;
        boolean isIllegalArgumentExceptionEx = false;
        try {
            Assert.assertTrue(customArray.addAll(10, arrStr));
        } catch (ArrayIndexOutOfBoundsException ex){
            isArrayIndexOutOfBoundsExceptionEx = true;
        }
        Assert.assertTrue(isArrayIndexOutOfBoundsExceptionEx);

        try {
            Assert.assertFalse(customArray.addAll(1, arrEmpty));
        } catch (IllegalArgumentException ex){
            isIllegalArgumentExceptionEx = true;
        }
        Assert.assertFalse(isIllegalArgumentExceptionEx);
    }

    @Test
    public void removeIndexTest(){
        CustomArrayImpl<Integer> arr = new CustomArrayImpl(Arrays.asList(new Integer[]{1, 20, 10}));
        arr.remove(1);
        Assert.assertEquals(2,arr.size());
    }

    @Test
    public void removeItemTest(){
        CustomArrayImpl<String> customArray = new CustomArrayImpl();
        String[] arrStr = {"a", "b", "c", "d"};
        customArray.addAll(arrStr);
        Assert.assertTrue(customArray.remove("a"));
        Assert.assertEquals("b", customArray.get(0));
    }

    @Test
    public void toArrayTest(){
        CustomArrayImpl<String> customArray = new CustomArrayImpl();
        String[] arrStr = {"a", "b", "c", "d"};
        customArray.addAll(arrStr);
        Assert.assertEquals(arrStr, customArray.toArray());
    }
    
    @Test  
    public void setTest(){
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        Integer[] arrInt = {11, 22, 33, 44, 55};
        customArray.addAll(arrInt);
        Integer numItem = 100;
        Assert.assertEquals(numItem, customArray.set(0, numItem));

        boolean isArrayIndexOutOfBoundsExceptionEx = false;
        try {
            Assert.assertEquals(numItem, customArray.set(10, numItem));
        } catch (ArrayIndexOutOfBoundsException ex){
            isArrayIndexOutOfBoundsExceptionEx = true;
        }
        Assert.assertTrue(isArrayIndexOutOfBoundsExceptionEx);
    }

    @Test
    public void getTest(){
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        Integer[] arrInt = {11, 22, 33, 44, 55};
        customArray.addAll(arrInt);
        Integer numItem = 11;
        Assert.assertEquals(numItem, customArray.get(0));

        boolean isArrayIndexOutOfBoundsExceptionEx = false;
        try {
            Assert.assertEquals(numItem, customArray.get(150));
        } catch (ArrayIndexOutOfBoundsException ex){
            isArrayIndexOutOfBoundsExceptionEx = true;
        }
        Assert.assertTrue(isArrayIndexOutOfBoundsExceptionEx);
    }
}
