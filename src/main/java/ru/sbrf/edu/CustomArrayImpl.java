package ru.sbrf.edu;

import java.util.Arrays;
import java.util.Collection;

public class CustomArrayImpl<T> implements CustomArray<T> {
    private Object[] array;
    private int capacity = 2;
    private int size = 0;

    public CustomArrayImpl() {
        array = new Object[10];
    }

    public CustomArrayImpl(int capacity) {
        this.capacity = capacity;
        array = new Object[capacity];
    }

    public CustomArrayImpl(Collection<T> c) {
        this.size = c.size();
        array = new Object[size];
    }

    public int size() {
        if (array == null) return 0;
        return size;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public boolean add(T item) {
        if (size > (int) (array.length / 1.25) + 1) {
            array = Arrays.copyOf(array, array.length * capacity);
        }
        array[size++] = item;
        return true;
    }

    public boolean addAll(T[] items) {
        if (items == null) {
            throw new IllegalArgumentException("Передан пустой массив");
        }
        for (T item : items) {
            if (item == null) throw new IllegalArgumentException("Передан пустой элемент");
            add(item);
        }
        return true;
    }

    public boolean addAll(Collection<T> items) {
        if (items == null) throw new IllegalArgumentException();
        for (T item : items) {
            if (item == null) throw new IllegalArgumentException();
            add(item);
        }
        return true;
    }

    public boolean addAll(int index, T[] items) {
        if (index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Индекс выходит за пределы");
        }
        if (items == null){
            throw new IllegalArgumentException("Передан пустой массив");
        }
        try {
            Object[] tempArr = this.array;
            if (index > 0) {
                this.array = Arrays.copyOf(Arrays.copyOfRange(tempArr, 0, index - 1), this.array.length + items.length);
            }
            addAll(items);
            for(int i = index + 1; i < tempArr.length; i++){
                this.array[i + items.length] = tempArr[i];
            }
            return true;
        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }

    public T get(int index) {
        if (index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Индекс выходит за пределы");
        }
        return (T) this.array[index];
    }

    public T set(int index, T item) {
        array[index] = item;
        return item;
    }

    public void remove(int index) {
        if (index < 0 || index >= size) return;
        for (int i = index; i < size; i++) {
            array[i] = array[i+1];
        }
        size--;
    }

    public boolean remove(T item) {
        int index = indexOf(item);
        if (index < 0 || index >= size) return false;
        for (int i = index; i < size; i++) {
            array[i] = array[i+1];
        }
        size--;
        return true;
    }

    public boolean contains(T item) {
        return false;
    }

    public int indexOf(T item) {
        for (int i = 0; i < size; i++) {
            if(item.equals(array[i])){
                return i;
            }
        }
        return -1;
    }

    public void ensureCapacity(int newElementsCount) {
        capacity += newElementsCount;
    }

    public int getCapacity() {
        return capacity;
    }

    public void reverse() {
        Object[] temp = new Object[size];
        for (int i = size - 1, j = 0; i >= 0; i--, j++) {
            temp[j] = array[i];
        }
        array = Arrays.copyOf(temp, array.length);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < size; i++) {
            sb.append(" ").append(array[i]);
        }
        sb.append(" ]");
        return sb.toString();
    }

    public Object[] toArray() {
        return Arrays.copyOf(array, size);
    }
}
